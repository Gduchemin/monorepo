import { sayHello } from "./hello";

test("say hello", () => {
  const logger = jest.spyOn(console, "log");
  sayHello();

  expect(logger).toHaveBeenCalledWith("Hello, World!");
  expect(logger).toHaveBeenCalledTimes(1);
});

test("say hello with name", () => {
  const logger = jest.spyOn(console, "log");
  sayHello("Fixy");

  expect(logger).toHaveBeenCalledWith("Hello, Fixy!");
  expect(logger).toHaveBeenCalledTimes(1);
});
